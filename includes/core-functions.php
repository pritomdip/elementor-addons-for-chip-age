<?php
//function prefix eaca

add_action( 'after_setup_theme', 'eaca_add_custom_image_size' );

function eaca_add_custom_image_size() {
    add_image_size( 'ca_slider', 760, 350, true );
}

function eat_get_toiree_widgets(){
	$widgets = array(
		'ca-slider' 	 => 'ca-slider.php',
		'ca-table'  	 => 'ca-table-content.php',
		'ca-image-hover' => 'ca-image-hover.php',
	);

	apply_filters( 'eat_get_toiree_widgets_after', $widgets );
	return $widgets;
}
