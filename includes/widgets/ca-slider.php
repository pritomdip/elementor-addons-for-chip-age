<?php
namespace pritom\ElementorAddonsForChipAge;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Plugin;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Repeater;

class CaSlider extends Widget_Base {
	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ca_slider';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'CA Slider', 'elementor-addons-for-chip-age' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-sliders';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'basic' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
            'ca_image',
            [
                'label' => __( 'Image Section', 'elementor-addons-for-chip-age' ),
                'tab'   => Controls_Manager::TAB_CONTENT,
            ]
        );

		$this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name'      => 'thumbnail',
                'default'   => 'large',
            ]
        );

        /**
         * Image
         */
        $this->add_control(
            'featured_image',
            [
                'label'   => __( 'Featured Image', 'elementor-addons-for-chip-age' ),
                'type'    => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater = new Repeater();

        $repeater->add_control(
			'image1', 
			[
				'label' => __( 'Title', 'elementor-addons-for-chip-age' ),
				'label'   => __( 'Image', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

        $this->add_control(
			'list',
			[
				'label'   => __( 'Repeater List', 'elementor-addons-for-chip-age' ),
				'type'    => Controls_Manager::REPEATER,
				'fields'  => $repeater->get_controls(),
				'default' => [
					[
                        'image1'  => [
                            'url' => Utils::get_placeholder_image_src(),
                        ],
                    ],
				],
				'title_field' 	  =>  __( 'Image', 'elementor-addons-for-chip-age' ),
			]
		);
        $this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

        $featured_photo     = $this->get_settings( 'featured_image' );
        $featured_photo_url = Group_Control_Image_Size::get_attachment_image_src( $featured_photo['id'], 'thumbnail', $settings );
        $featured_photo_url = empty( $featured_photo_url ) ? $featured_photo['url'] : $featured_photo_url;
        $featured_thumbnail_size_url =  wp_get_attachment_image_url( $featured_photo['id'], 'thumbnail' );
		?>
		
		<div class="ca-slider-wrapper">
		  	<div class="ca-slider">
			    <div><img src="<?php echo $featured_photo_url; ?>" /></div>
			    <?php 
			    if( !empty( $settings['list'] ) ){
			    	foreach( $settings['list'] as $slide ){

			    		$image_url = Group_Control_Image_Size::get_attachment_image_src( $slide['image1']['id'], 'thumbnail', $settings );
    					$img_url   = empty( $image_url ) ? $slide['image1']['url'] :$image_url;

		                if ( $img_url ) {
		                    
				        ?>
				        <div>
				        	<img src="<?php echo $img_url; ?>" />
				        </div>

				        <?php
		                }
			    	}
			    }
			    ?>

			</div>
		  	<div class="ca-slider-nav" style="display:none;">

			    <div><h3>1</h3></div>

			    <?php 
			    if( !empty( $settings['list'] ) ){
			    	$count = 2;
			    	foreach( $settings['list'] as $slide ){      
					?>
				    	<div><h3><?php echo $count; ?></h3></div>
					<?php
				    $count++;
			    	}
			    }
			    ?>
			</div>

		  	<div class="ca-action">

			  	<a href="#" class="ca-opacity" data-slide="1"><img src="<?php echo $featured_thumbnail_size_url; ?>" /></a>
			  	<?php 
			    if( !empty( $settings['list'] ) ){
			    	$count = 2;
			    	foreach( $settings['list'] as $slide ){ 
			    		$image_url = wp_get_attachment_image_url( $slide['image1']['id'], 'thumbnail' );
			    		if( !empty( $image_url )) {
						?>
				    		<a href="#" data-slide="<?php echo $count; ?>">
				    			<img src="<?php echo $image_url; ?>" />
				    		</a>
				    	<?php
						}
				    $count++;
			    	}
			    }
			    ?>
		  	</div>

		</div>
	<?php
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new CaSlider() );