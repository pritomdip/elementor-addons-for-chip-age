<?php
namespace pritom\ElementorAddonsForChipAge;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Plugin;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;

class CaImageHover extends Widget_Base {
	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ca_image_hover';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Toiree Image Hover Animation', 'elementor-addons-for-chip-age' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-picture-o';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'basic' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'ca_img_hover',
			[
				'label'	=> 'Image Section',
				'tab'	=> Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'		=> 'ca-img-hov',
				'default'   => 'large',
			]
			
		);

		$this->add_control(
            'feature_image',
            [
                'label'   => __( 'Featured Image', 'elementor-addons-for-chip-age' ),
                'type'    => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
            ]
        );

		$this->end_controls_section();

		$this->start_controls_section(
			'ca_img_content',
			[
				'label'	=> 'Content',
				'tab'	=> Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'short_desc',
			[
				'label'		=> __('Short Description', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::TEXT,
				'default'	=> __('This is short description', 'elementor-addons-for-chip-age'),
			]
		);

		$this->add_control(
			'title',
			[
				'label'		=> __('Title', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::TEXT,
				'default'	=> __('This is heading', 'elementor-addons-for-chip-age'),
			]
		);

		$this->add_control(
			'desc',
			[
				'label'		=> __('Description', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::TEXT,
				'default'	=> __('This is description', 'elementor-addons-for-chip-age'),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Content Style', 'elementor-addons-for-chip-age' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_bg_clr',
			[
				'label' 	=> __('Background Color', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::COLOR,
				'default'	=> '#f1f1f1',
				'selectors' => [
					'{{WRAPPER}} .ca-img-title, {{WRAPPER}} .ca-img-desc' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_font_clr',
			[
				'label' 	=> __('Title Font Color', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::COLOR,
				'default'	=> '#555',
				'selectors' => [
					'{{WRAPPER}} .ca-img-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'desc_font_clr',
			[
				'label' 	=> __('Description Font Color', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::COLOR,
				'default'	=> '#555',
				'selectors' => [
					'{{WRAPPER}} .ca-img-desc' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'short_desc_font_clr',
			[
				'label' 	=> __('Short Description Font Color', 'elementor-addons-for-chip-age'),
				'type'		=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ca-short-desc' => 'color:{{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' 	=> __('Title Typography', 'elementor-addons-for-chip-age'),
				'name' 		=> 'title_typography',
				'selector' 	=> '{{WRAPPER}} .ca-img-title',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' 	=> __('Description Typography', 'elementor-addons-for-chip-age'),
				'name' 		=> 'desc_typography',
				'selector' 	=> '{{WRAPPER}} .ca-img-desc',
				'scheme' 	=> Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();
        ?>

		<div class="ca-container">
		  	<div class="ca-grid">
			    <figure class="ca-img-hover-effect ca-content">
			      <a href="">
			        <img src="https://images.unsplash.com/photo-1492562080023-ab3db95bfbce?auto=format&fit=crop&w=731&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D 731w" />

			        <div class="ca-img-content-wrapper">

			        	<?php 
			        	if(!empty($settings['short_desc'])){
			        	?>
		        			<div class="ca-content-details ca-fadeIn-left">
					            <p class="ca-short-desc"><?php echo $settings['short_desc']; ?></p>
					        </div>
			        	<?php
			        	}
			        	?>

			          

			          <h2 class="ca-img-title"> <?php echo $settings['title']; ?> </h2>
			          <p class="ca-img-desc"><?php echo $settings['desc']; ?></p>
			        </div>
			      </a>
			    </figure>
			 </div>
		</div>

        <?php
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new CaImageHover() );