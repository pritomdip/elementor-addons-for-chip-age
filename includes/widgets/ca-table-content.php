<?php
namespace pritom\ElementorAddonsForChipAge;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Plugin;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

class TableContent extends Widget_Base{

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'ca-table';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Toiree Room Aminities', 'elementor-addons-for-chip-age' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-table';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'basic' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'content_table_header',
			[
				'label' => __( 'Table Header', 'elementor-addons-for-chip-age' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'table_header',
			[
				'label' => __( 'Table Header Cell', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'text' => __( 'Table Header', 'elementor-addons-for-chip-age' ),
					],
					[
						'text' => __( 'Table Header', 'elementor-addons-for-chip-age' ),
					]
				],
				'fields' => [
					[
						'name' => 'text',
						'label' => __( 'Text', 'elementor-addons-for-chip-age' ),
						'type' => Controls_Manager::TEXT,
						'label_block' => true,
						'placeholder' => __( 'table data', 'elementor-addons-for-chip-age' ),
						'default' => __( 'table data', 'elementor-addons-for-chip-age' ),
						'dynamic' => [
		                    'active' => true,
		                ]
					],
					[
						'name'	=> 'advance',
						'label' => __( 'Advance Settings', 'elementor-addons-for-chip-age' ),
						'type' => Controls_Manager::SWITCHER,
						'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
						'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
					],
					[
						'name'	=> 'colspan',
						'label' => __( 'ColSpan', 'elementor-addons-for-chip-age' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'advance' => 'yes',
						],
						'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
						'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
					],
					[
						'name'	=> 'colspannumber',
						'label' => __( 'colSpan Number', 'elementor' ),
						'type' => Controls_Manager::TEXT,
						'condition' => [
							'advance' => 'yes',
							'colspan' => 'yes',
						],
						'placeholder' => __( '1', 'elementor-addons-for-chip-age' ),
						'default' => __( '1', 'elementor-addons-for-chip-age' ),
					],
					[
						'name'	=> 'customwidth',
						'label' => __( 'Custom Width', 'elementor-addons-for-chip-age' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'advance' => 'yes',
						],
						'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
						'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
					],
					[
						'name'	=> 'width',
						'label' => __( 'Width', 'elementor' ),
						'type' => Controls_Manager::SLIDER,
						'condition' => [
							'advance' => 'yes',
							'customwidth' => 'yes',
						],
						'range' => [
							'%' => [
								'min' => 0,
								'max' => 100,
							],
							'px' => [
								'min' => 1,
								'max' => 1000,
							],
						],
						'default' => [
							'size' => 30,
							'unit' => '%',
						],
						'size_units' => [ '%', 'px' ],
						'selectors' => [ '{{WRAPPER}} table.ca-table {{CURRENT_ITEM}}' => 'width: {{SIZE}}{{UNIT}};',
						]
					],
					[
						'name' => 'align', 
						'label' => __( 'Alignment', 'elementor-addons-for-chip-age' ),
						'type' => Controls_Manager::CHOOSE,
						'condition' => [
							'advance' => 'yes',
						],
						'options' => [
							'left' => [
								'title' => __( 'Left', 'elementor-addons-for-chip-age' ),
								'icon' => 'fa fa-align-left',
							],
							'center' => [
								'title' => __( 'Center', 'elementor-addons-for-chip-age' ),
								'icon' => 'fa fa-align-center',
							],
							'right' => [
								'title' => __( 'Right', 'elementor-addons-for-chip-age' ),
								'icon' => 'fa fa-align-right',
							],
							'justify' => [
								'title' => __( 'Justified', 'elementor-addons-for-chip-age' ),
								'icon' => 'fa fa-align-justify',
							],
						],
						'default' => '',
						'selectors' => [
							'{{WRAPPER}} table.ca-table {{CURRENT_ITEM}}' => 'text-align: {{VALUE}};',
						]
					],
					[
						'name'	=> 'decoration',
						'label' => __( 'Decoration', 'elementor-addons-for-chip-age' ),
						'type' => Controls_Manager::SELECT,
						'condition' => [
							'advance' => 'yes',
						],
						'options' => [
							''  => __( 'Default', 'elementor-addons-for-chip-age' ),
							'underline' => __( 'Underline', 'elementor-addons-for-chip-age' ),
							'overline' => __( 'Overline', 'elementor-addons-for-chip-age' ),
							'line-through' => __( 'Line Through', 'elementor-addons-for-chip-age' ),
							'none' => __( 'None', 'elementor-addons-for-chip-age' ),
						],
						'default' => '',
						'selectors' => [
							'{{WRAPPER}} table.ca-table {{CURRENT_ITEM}}' => 'text-decoration: {{VALUE}};',
						],
					]
				],
				'title_field' => '{{{ text }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'content_table_body',
			[
				'label' => __( 'Table Body', 'elementor-addons-for-chip-age' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'row', [
				'label' => __( 'New Row', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
				'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
			]
		);

		$repeater->add_control(
			'text', [
				'label' => __( 'Text', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'placeholder' => __( 'Table Data', 'elementor-addons-for-chip-age' ),
				'default' => __( 'Table Data', 'elementor-addons-for-chip-age' ),
				'dynamic' => [
		            'active' => true,
		        ]
			]
		);

		$repeater->add_control(
			'icon',
			[
				'label' => __( 'Social Icons', 'plugin-domain' ),
				'type' => Controls_Manager::ICON,
				
				'default' => 'fa fa-facebook',
			]
		);

		$repeater->add_control(
			'advance', [
				'label' => __( 'Advance Settings', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
				'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
			]
		);

		$repeater->add_control(
			'colspan', [
				'label' => __( 'colSpan', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::SWITCHER,
				'condition' => [
					'advance' => 'yes',
				],
				'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
				'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
			]
		);

		$repeater->add_control(
			'colspannumber', [
				'label' => __( 'colSpan Number', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'condition' => [
					'advance' => 'yes',
					'colspan' => 'yes',
				],
				'placeholder' => __( '1', 'elementor-addons-for-chip-age' ),
				'default' => __( '1', 'elementor-addons-for-chip-age' ),
			]
		);

		$repeater->add_control(
			'rowspan', [
				'label' => __( 'rowSpan', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::SWITCHER,
				'condition' => [
					'advance' => 'yes',
				],
				'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
				'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
			]
		);

		$repeater->add_control(
			'rowspannumber', [
				'label' => __( 'rowSpan Number', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'condition' => [
					'advance' => 'yes',
					'rowspan' => 'yes',
				],
				'placeholder' => __( '1', 'elementor-addons-for-chip-age' ),
				'default' => __( '1', 'elementor-addons-for-chip-age' ),
			]
		);

		$repeater->add_control(
			'align', [
				'label' => __( 'Alignment', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::CHOOSE,
				'condition' => [
					'advance' => 'yes',
				],
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.ca-table {{CURRENT_ITEM}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$repeater->add_control(
			'decoration',
			[
				'label' => __( 'Decoration', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::SELECT,
				'condition' => [
					'advance' => 'yes',
				],
				'options' => [
					''  => __( 'Default', 'elementor-addons-for-chip-age' ),
					'underline' => __( 'Underline', 'elementor-addons-for-chip-age' ),
					'overline' => __( 'Overline', 'elementor-addons-for-chip-age' ),
					'line-through' => __( 'Line Through', 'elementor-addons-for-chip-age' ),
					'none' => __( 'None', 'elementor-addons-for-chip-age' ),
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.ca-table {{CURRENT_ITEM}}' => 'text-decoration: {{VALUE}};',
				],
			]
		);

		/*---
		$repeater->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'td_typography',
				'label' => __( 'Typography', 'elementor-addons-for-chip-age' ),
				'selector' => '{{WRAPPER}} table.ca-table {{CURRENT_ITEM}}',
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_4,
			]
		);
		----*/

		//$repeater->end_controls_tab();
		//$repeater->end_controls_tabs();


		$this->add_control(
			'table_body',
			[
				'label' => __( 'Table Body Cell', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'text' => __( 'Table Data', 'elementor-addons-for-chip-age' ),
					],
					[
						'text' => __( 'Table Data', 'elementor-addons-for-chip-age' ),
					],
				],
				'title_field' => '{{{ text }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'General Style', 'elementor-addons-for-chip-age' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'table_padding',
			[
				'label' => __( 'Inner Cell Padding', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} table.ca-table td,{{WRAPPER}} table.ca-table th' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'table_border',
				'label' => __( 'Border', 'elementor-addons-for-chip-age' ),
				'selector' => '{{WRAPPER}} table.ca-table td,{{WRAPPER}} table.ca-table th',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'table_header_style',
			[
				'label' => __( 'Table Header Style', 'elementor-addons-for-chip-age' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'header_align',
			[
				'label' => __( 'Alignment', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-header' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'header_text_color',
			[
				'label' => __( 'Text Color', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-header' => 'color: {{VALUE}};',
				]
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'header_typography',
				'selector' => '{{WRAPPER}} table.ca-table .ca-table-header',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$this->add_control(
			'header_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-header' => 'background-color: {{VALUE}};',
				]
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'table_body_style',
			[
				'label' => __( 'Table Body Style', 'elementor-addons-for-chip-age' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'body_align',
			[
				'label' => __( 'Alignment', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elementor-addons-for-chip-age' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-body' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'body_text_color',
			[
				'label' => __( 'Text Color', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-body' => 'color: {{VALUE}};',
				]
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'body_typography',
				'selector' => '{{WRAPPER}} table.ca-table .ca-table-body',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$this->add_control(
			'body_bg_color',
			[
				'label' => __( 'Background Color', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-body' => 'background-color: {{VALUE}};',
				]
			]
		);

		$this->add_control(
			'striped_bg', 
			[
				'label' => __( 'Striped Background', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'No', 'elementor-addons-for-chip-age' ),
				'label_on' => __( 'Yes', 'elementor-addons-for-chip-age' ),
			]
		);
		$this->add_control(
			'striped_bg_color', 
			[
				'label' => __( 'Secondary Background Color', 'elementor-addons-for-chip-age' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'striped_bg' => 'yes',
				],
				'selectors' => [
					'{{WRAPPER}} table.ca-table .ca-table-body tr:nth-of-type(2n)' => 'background-color: {{VALUE}};',
				]
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();
		?>
		<table class="ca-table">
			<thead  class="ca-table-header">
				<tr>
					<?php
					foreach ($settings['table_header'] as $index => $item) {
						$repeater_setting_key = $this->get_repeater_setting_key( 'text', 'table_header', $index );
						
						$this->add_inline_editing_attributes( $repeater_setting_key );

						$colspan = ($item['colspan'] == 'yes' && $item['advance'] == 'yes') ? 'colSpan="'.$item['colspannumber'].'"' : '';

						if( !empty($item['text'] ) ){
							echo '<th class="elementor-inline-editing elementor-repeater-item-'.$item['_id'].'"  '.$colspan.' '.$this->get_render_attribute_string( $repeater_setting_key ).'>'.$item['text'].'</th>';
						}
					}
					?>
				</tr>
			</thead>
			<tbody class="ca-table-body">
				<tr>
					<?php
					foreach ( $settings['table_body'] as $index => $item ) {
						$table_body_key = $this->get_repeater_setting_key( 'text', 'table_body', $index );

						$this->add_render_attribute( $table_body_key, 'class', 'elementor-repeater-item-'.$item['_id'] );
						$this->add_inline_editing_attributes( $table_body_key );

						if( $item['row'] == 'yes' ){
							echo '</tr><tr>';
						}

						$colspan = ($item['colspan'] == 'yes' && $item['advance'] == 'yes') ? 'colSpan="'.$item['colspannumber'].'"' : '';

						$rowspan = ($item['rowspan'] == 'yes' & $item['advance'] == 'yes') ? 'rowSpan="'.$item['rowspannumber'].'"' : '';

						echo '<td '.$colspan.' '.$rowspan.' '.$this->get_render_attribute_string( $table_body_key ).' ><i class=" ca-icons ' . $item['icon'] . '" aria-hidden="true"></i>'.$item['text'].'</td>';
					}
					?>
				</tr>
			</tbody>
		</table>
		
		<?php

	}

	protected function _content_template() {
		?>
		<table class="ca-table">
			<thead class="ca-table-header">
				<tr>
					<#
					if ( settings.table_header ) {
						_.each( settings.table_header, function( item, index ) {
							var iconTextKey = view.getRepeaterSettingKey( 'text', 'table_header', index );

							if( 'yes' === item.colspan && 'yes' === item.advance){
								colSpan = 'colSpan="'+item.colspannumber+'"';
							}else{
								colSpan = '';
							}
							
							view.addRenderAttribute( iconTextKey, 'class', 'elementor-repeater-item-'+item._id );
							view.addInlineEditingAttributes( iconTextKey );
							#>
							<th {{{colSpan}}} {{{ view.getRenderAttributeString( iconTextKey ) }}}>{{{ item.text }}}</th>
						<#
						} );
					} #>
				</tr>
			</thead>
			<tbody class="ca-table-body">
				<tr>
					<#
					if ( settings.table_body ) {
						_.each( settings.table_body, function( item, index ) {
							if( 'yes' === item.row){
								newRow = '</tr><tr>';
							}else{
								newRow = '';
							}

							if( 'yes' === item.colspan && 'yes' === item.advance){
								colSpan = 'colSpan="'+item.colspannumber+'"';
							}else{
								colSpan = '';
							}

							if( 'yes' === item.rowspan && 'yes' === item.advance){
								rowSpan = 'rowSpan="'+item.rowspannumber+'"';
							}else{
								rowSpan = '';
							}

							var tdTextKey = view.getRepeaterSettingKey( 'text', 'table_body', index );
							
							view.addRenderAttribute( tdTextKey, 'class', 'elementor-repeater-item-'+item._id );
							view.addInlineEditingAttributes( tdTextKey );

						
							var icon = item.icon;

							view.addRenderAttribute( icon, 'class', 
							'ca-icons ' + icon ); 
							view.addInlineEditingAttributes( icon );
							#>
							{{{newRow}}}

							<td {{{rowSpan}}} {{{colSpan}}} {{{ view.getRenderAttributeString( tdTextKey ) }}}><i {{{ view.getRenderAttributeString( icon ) }}}></i>{{{ item.text }}}</td>
						<#
						} );
					} #>
				</tr>
			</tbody>
		</table>
		<?php
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new TableContent() );