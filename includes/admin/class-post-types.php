<?php

namespace Pritom\ElementorAddonsForChipAge\Admin;

class PostTypes {
    /**
     * PostTypes constructor.
     */
    public function __construct() {
        // add_action( 'init', array( $this, 'register_post_types' ) );
        // add_action( 'init', array( $this, 'register_taxonomies' ) );
    }

    /**
     * Register custom post types
     */
    public function register_post_types() {
        register_post_type( 'ca_rooms', array(
            'labels'              => $this->get_posts_labels( 'Rooms', __( 'Room', 'elementor-addons-for-chip-age' ), __( 'Rooms', 'elementor-addons-for-chip-age' ) ),
            'hierarchical'        => false,
            'supports'            => array(  'title', 'editor', 'thumbnail' ),
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-image-flip-horizontal',
            'publicly_queryable'  => true,
            'exclude_from_search' => false,
            'has_archive'         => true,
            'query_var'           => true,
            'can_export'          => true,
            'rewrite'             => true,
            'capability_type'     => 'post',
        ) );

    }

	/**
	 * Register custom taxonomies
	 *
	 * @since 1.0.0
	 */
    public function register_taxonomies() {
        register_taxonomy( 'ca_rooms_category', array( 'ca_rooms' ), array(
            'hierarchical'      => true,
            'labels'            => $this->get_posts_labels( 'Room Category', __( 'Room Category', 'elementor-addons-for-chip-age' ), __( 'Room Categories', 'elementor-addons-for-chip-age' ) ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
        ) );

    }

	/**
	 * Get all labels from post types
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
    protected static function get_posts_labels( $menu_name, $singular, $plural ) {
        $labels = array(
            'name'               => $singular,
            'all_items'          => sprintf( __( "All %s", 'elementor-addons-for-chip-age' ), $plural ),
            'singular_name'      => $singular,
            'add_new'            => sprintf( __( 'New %s', 'elementor-addons-for-chip-age' ), $singular ),
            'add_new_item'       => sprintf( __( 'Add New %s', 'elementor-addons-for-chip-age' ), $singular ),
            'edit_item'          => sprintf( __( 'Edit %s', 'elementor-addons-for-chip-age' ), $singular ),
            'new_item'           => sprintf( __( 'New %s', 'elementor-addons-for-chip-age' ), $singular ),
            'view_item'          => sprintf( __( 'View %s', 'elementor-addons-for-chip-age' ), $singular ),
            'search_items'       => sprintf( __( 'Search %s', 'elementor-addons-for-chip-age' ), $plural ),
            'not_found'          => sprintf( __( 'No %s found', 'elementor-addons-for-chip-age' ), $plural ),
            'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'elementor-addons-for-chip-age' ), $plural ),
            'parent_item_colon'  => sprintf( __( 'Parent %s:', 'elementor-addons-for-chip-age' ), $singular ),
            'menu_name'          => $menu_name,
        );

        return $labels;
    }

	/**
	 * Get all labels from taxonomies
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
    protected static function get_taxonomy_label( $menu_name, $singular, $plural ) {
        $labels = array(
            'name'              => sprintf( _x( '%s', 'taxonomy general name', 'elementor-addons-for-chip-age' ), $plural ),
            'singular_name'     => sprintf( _x( '%s', 'taxonomy singular name', 'elementor-addons-for-chip-age' ), $singular ),
            'search_items'      => sprintf( __( 'Search %', 'elementor-addons-for-chip-age' ), $plural ),
            'all_items'         => sprintf( __( 'All %s', 'elementor-addons-for-chip-age' ), $plural ),
            'parent_item'       => sprintf( __( 'Parent %s', 'elementor-addons-for-chip-age' ), $singular ),
            'parent_item_colon' => sprintf( __( 'Parent %s:', 'elementor-addons-for-chip-age' ), $singular ),
            'edit_item'         => sprintf( __( 'Edit %s', 'elementor-addons-for-chip-age' ), $singular ),
            'update_item'       => sprintf( __( 'Update %s', 'elementor-addons-for-chip-age' ), $singular ),
            'add_new_item'      => sprintf( __( 'Add New %s', 'elementor-addons-for-chip-age' ), $singular ),
            'new_item_name'     => sprintf( __( 'New % Name', 'elementor-addons-for-chip-age' ), $singular ),
            'menu_name'         => __( $menu_name, 'elementor-addons-for-chip-age' ),
        );

        return $labels;
    }
}
