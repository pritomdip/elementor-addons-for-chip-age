/**
 * Elementor Addons For Chip Age 
 *
 * Copyright (c) 2019 Pritom Chowdhury Dip
 * Licensed under the GPLv2+ license.
 */
/*jslint browser: true */
/*global jQuery:false */

'use strict';

(function ($, w) {
    var $window = $(w);

    $window.on('elementor/frontend/init', function() {
        var EF = elementorFrontend,
            EM = elementorModules;

        var Slider = EM.frontend.handlers.Base.extend({
            onInit: function () {
                EM.frontend.handlers.Base.prototype.onInit.apply(this, arguments);
                this.chipAgeInitSlider();
            },
            chipAgeInitSlider: function(){
	            $('.ca-slider').slick({
				   slidesToShow: 1,
				   slidesToScroll: 1,
				   arrows: true,
				   fade: true,
				   asNavFor: '.ca-slider-nav'
				});
				$('.ca-slider-nav').slick({
				   slidesToShow: 3,
				   slidesToScroll: 1,
				   asNavFor: '.ca-slider',
				   dots: true,
				   focusOnSelect: true
				});

				$('a[data-slide]').click(function(e) {
				   e.preventDefault();
				   var slideno = $(this).data('slide');
				   $('.ca-slider-nav').slick('slickGoTo', slideno - 1);
                   $(this).siblings().removeClass('ca-opacity');
                   $(this).addClass('ca-opacity'); 
				});
            }
        });

        var handlersClassMapping = {
            'ca_slider.default' : Slider
        };

        $.each( handlersClassMapping, function( widgetName, handlerClass ) {
            EF.hooks.addAction( 'frontend/element_ready/' + widgetName, function( $scope ) {
                EF.elementsHandler.addHandler( handlerClass, { $element: $scope });
            });
        });
    });

} (jQuery, window));
